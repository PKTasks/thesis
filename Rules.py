#rules
import Structures
import math
import sys
import datetime as dt
from argparse import ArgumentParser
import GeneticFunctions
import random

def rule1Check(penalty, solutions, pilots, numberOfChromosomes, earlierDateGiven, latestDateGiven, flightDays):  #na uparxoun 2 hmerologiakes meres free mesa se mia evdomada gia kathe kubernhth
	# penalty = [0 for x in range(numberOfChromosomes)]
	solutionNo = 0

	for solution in pilots:

		for pilot in solution:

			for SP in pilot.SPArray:

				if SP.flightArray[0].depDatetime.hour > latestDateGiven.hour:
					# print SP.flightArray[0].depDatetime," ", latestDateGiven ," ",((latestDateGiven - SP.flightArray[0].depDatetime).days + 1)
					aa = ((latestDateGiven - SP.flightArray[0].depDatetime).days + 1)

				elif SP.flightArray[0].depDatetime.hour == latestDateGiven.hour:

					if SP.flightArray[0].depDatetime.minute > latestDateGiven.minute:
						# print SP.flightArray[0].depDatetime," ", latestDateGiven ," ",((latestDateGiven - SP.flightArray[0].depDatetime).days + 1)
						aa = ((latestDateGiven - SP.flightArray[0].depDatetime).days + 1)
					else:
						# print SP.flightArray[0].depDatetime," ", latestDateGiven ," ",(latestDateGiven - SP.flightArray[0].depDatetime).days
						aa = (latestDateGiven - SP.flightArray[0].depDatetime).days

				else:
					# print SP.flightArray[0].depDatetime," ", latestDateGiven ," ",(latestDateGiven - SP.flightArray[0].depDatetime).days
					aa = (latestDateGiven - SP.flightArray[0].depDatetime).days
				# print flightDays,aa, flightDays - aa -1
				pilot.rule1Array[flightDays - aa-1] = 1
			# print "\n"


			counter = 0
			numOfFlightDays = 0
			for x in xrange(0,flightDays):  #+8 gia na tsekarw tis 7 sunexomenes theseis tou pinaka
				counter += 1

				if pilot.rule1Array[x] == 1:
					numOfFlightDays += 1

				if counter == 7:
					if numOfFlightDays > 2:
						solutions[solutionNo].ruleViolation = 1
						penalty[solutionNo] = 1000
						break
					counter = 0

		solutionNo+=1
	return penalty



			
#na mhn sumpaiftoun xronika 2 sp ston idio kubernhth
#na uparxoun 11 wres keno metaksu 2 sunexomenwn sp ston idio kubernhth
def rulesCheck(penalty, solutions, pilots, numberOfChromosomes, totalSPs, overlapArr, spIdOverlapArr, elevenHoursRestArr):
	penalty = [0 for x in range(numberOfChromosomes)]
	solutionNo = 0

	for solution in pilots:
		# print "solution", solutionNo +1
		for pilot in solution:

			tempSPARray = pilot.SPArray[:]
			# print "	pilot id:", pilot.pilotId

			for SP1 in pilot.SPArray:

				############################
				i = 0
				for x in spIdOverlapArr:
					# print x, int(SP1.spId), i
					i +=1
					if x == int(SP1.spId):
						break
				
				for SP2 in tempSPARray:
					# print SP2.spId
					if int(SP2.spId) != int(SP1.spId):    #!!!!ksana koita!!!!##########################################sos 
						if int(SP2.spId) in overlapArr[i - 1]:
							# print "		true"
							solutions[solutionNo].ruleViolation = 1
							penalty[solutionNo] = 1000
							break

							# print "		false"
						if int(SP2.spId) in elevenHoursRestArr[i-1]:
							solutions[solutionNo].ruleViolation = 1
							penalty[solutionNo] = 1000
							break

		solutionNo+=1

	return penalty




def findSPTimeOverlap11HoursRest(overlapArr, elevenHoursRestArr, SPList):
	overlapArr=[]
	spIdOverlapArr=[]
	elevenHoursRestArr=[]

	for SP1 in SPList:
		a_list = []
		b_list = []
		spIdOverlapArr.append(int(SP1.spId))

		##########################
		a_list.append(int(SP1.spId))
		b_list.append(int(SP1.spId))
		##########################
		for SP2 in SPList:

			if SP1.spId != SP2.spId:

				firstSPDate1 = SP1.flightArray[0].depDatetime
				lastSPDate1 = SP1.flightArray[-1].arrDatetime

				firstSPDate2 = SP2.flightArray[0].depDatetime
				lastSPDate2 = SP2.flightArray[-1].arrDatetime

				if firstSPDate1 < firstSPDate2:
					# print firstSPDate2 - firstSPDate1
					if lastSPDate1 > firstSPDate2:      #elegxos kanona epikalupshs

						a_list.append(int(SP2.spId))

					else:                               #elegxos kanona 11 wrwn

						bb = firstSPDate2 - lastSPDate1

						hoursUntilMidnight = float(24*60) - float(lastSPDate1.hour*60)- float((lastSPDate1.minute))
						hoursUntilMidnight = hoursUntilMidnight/60

						restDay = hoursUntilMidnight + float(24)  #float(48)

						hours = float(bb.days*24) + (float(bb.seconds) / float((60*60)))

						if hours < 11.0 or hours >= restDay:
							b_list.append(int(SP2.spId))



				elif firstSPDate2 < firstSPDate1:
					if lastSPDate2 > firstSPDate1:    #elegxos kanona epikalupshs

						a_list.append(int(SP2.spId))

					else:                             #elegxos kanona 11 wrwn

						bb = firstSPDate1 - lastSPDate2

						hoursUntilMidnight = float(24*60) - float(lastSPDate1.hour*60)- float((lastSPDate1.minute))
						hoursUntilMidnight = hoursUntilMidnight/60

						restDay = hoursUntilMidnight + float(24)  #float(48)

						hours = float(bb.days*24) + (float(bb.seconds) / float((60*60)))

						if hours < 11.0 or hours >= restDay:
							b_list.append(int(SP2.spId))



		overlapArr.append(a_list)
		elevenHoursRestArr.append(b_list)


	return overlapArr, spIdOverlapArr, elevenHoursRestArr


def printRule1Array(pilots):
	for solution in pilots:

		for pilot in solution:
			print pilot.rule1Array
		print "\n"


def printPenalty(penalty, numberOfChromosomes):
	for x in xrange(0,numberOfChromosomes):
		print "penalty: ",penalty[x]
