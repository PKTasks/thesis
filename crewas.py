import math
import sys
import datetime as dt
from argparse import ArgumentParser
import GeneticFunctions
import Structures
import Functions
import random
import Rules


mutationProbability = 0.05  #ligo katw to orizw ws 1 / totalSPs
selectionPercentage = 0.4
# selectionPercentage = 0.3

def main(argv):

	#**** diavazw ta orismata ****#
	parser = ArgumentParser()
	parser.add_argument("-p", "--file", dest="filename", help="write report to FILE", metavar="FILE")
	parser.add_argument("-n", metavar='N', type=int, nargs='+', help='an integer for the accumulator')
	parser.add_argument("-t", dest="numberOfChromosomes", type=int)    #einai o arithmos twn xrwsomatwn pou tha dhmiourghthoun
	parser.add_argument("-s", dest="starttime")
	parser.add_argument("-e", dest="endtime")
	parser.add_argument("-r", dest="seed")
	parser.add_argument("-o", dest="output")
	parser.add_argument("-c", dest="crossoverMethod")

	args = parser.parse_args()
	print args

	fileName = args.filename  #arxeio eisagwghs
	if fileName is None:                #elegxetai an dothhke h onoma arxeiou
		print "error: [you must give a starting file as an argument]"
		exit(10)

	numberOfChromosomes = args.numberOfChromosomes
	if numberOfChromosomes is None:
		print "error: [you must give a numberOfChromosomes for the chromosomes as an argument]"
		exit(10)


	startTime = args.starttime
	if startTime is not None:           #elegxetai an dothhke h param starttime

		startTime = startTime.split("/")
		startTimeGiven = startTime[0] + " " + startTime[1]
		startTimeGiven = dt.datetime.strptime(startTimeGiven, '%Y-%m-%d %H:%M')
		print "startTimeGiven is:", startTimeGiven

	else:
		startTimeGiven = dt.datetime.strptime("2001-01-01 00:00", '%Y-%m-%d %H:%M')
		print "starting time is:",startTime


	endTime = args.endtime
	if endTime is not None:           #elegxetai an dothhke h param endtime

		endTime = endTime.split("/")
		endTimeGiven = endTime[0] + " " + endTime[1]
		endTimeGiven = dt.datetime.strptime(endTimeGiven, '%Y-%m-%d %H:%M')
		print "endTimeGiven is:", endTimeGiven

	else:
		endTimeGiven = dt.datetime.strptime("2020-12-31 23:59", '%Y-%m-%d %H:%M')
		print "ending time is:",endTime

	if startTimeGiven > endTimeGiven:
		print "give enter start time earlier than end time"
		exit()

	seed = args.seed
	print "seed is:",seed
	if seed is not None:               #elegxetai an dothhke h param seed
		random.seed(a=int(seed))

	else:
		random.seed(a=None)


	outputFile = args.output
	print "outputFile is:",outputFile
	if outputFile is not None:               #elegxetai an dothhke h param outputFile
		outputGiven = 1

	crossoverMethod = args.crossoverMethod
	print "crossover method:" ,crossoverMethod
	if crossoverMethod is None:
		crossoverMethodGiven = 1
	else:

		if crossoverMethod == "single-point":
			crossoverMethodGiven = 1
		elif crossoverMethod == "two-point":
			crossoverMethodGiven = 2
		elif crossoverMethod == "uniform":
			crossoverMethodGiven = 3
		else:
			print "\n[wrong syntax on crossover]..\n options: single-point \n\t two-point"
			exit()


	try:
		N = args.n
		N = N[0]	#arithmos pilotwn
		print "N =", N
	except:
		print "error: [you must give a number of pilots as an argument]"
		exit(10)
	#****-------------------------****#


	# print "last pilot:",pilots[-1].pilotId

	dataFromFile = Functions.ReadFromFile(fileName)  #diavazw ta dedomena apo to arxeio


	#****-------------------------****#
	prevSpId = "0000"
	totalSPs = 0


	totalSPs = Functions.findTotalSP(dataFromFile, totalSPs, prevSpId, startTimeGiven, endTimeGiven)

	print "totalSPs:", totalSPs , "\n"

	mutationProbability = float(1) / float(totalSPs)
	print "mutation :", mutationProbability
	#****--------------------------****#


	#****-------------------------****# desmeuw listes gia pithanes listes kai pilotous(des readme)

	SPList, earlierDateGiven, latestDateGiven = Functions.spCreateList(dataFromFile, startTimeGiven, endTimeGiven)  #h lista me ola ta sp apo to arxeio

	solutions = Functions.solutionsCreateList(numberOfChromosomes, totalSPs)

	solutions = Functions.solutionsRandomInsert(solutions, N, numberOfChromosomes,totalSPs)

	pilots = Functions.pilotsCreateList(N, numberOfChromosomes, earlierDateGiven, latestDateGiven)

	pilots = Functions.setSPToPilots(solutions, pilots, N, numberOfChromosomes, totalSPs, SPList)

	#****-------------------------****#

	overlapArr=[]  #isws allaksei kai ginei lista apo listes gia oikonomia xwrou
	elevenHoursRestArr=[]
	overlapArr,spIdOverlapArr, elevenHoursRestArr = Rules.findSPTimeOverlap11HoursRest(overlapArr, elevenHoursRestArr, SPList)
	

	flightDays = Functions.totalFlightDays(earlierDateGiven, latestDateGiven)


	#****------------------------****#
	ift = GeneticFunctions.IFT(SPList, totalSPs, N)
	smallest, bestSolution, pilotsbest = GeneticFunctions.geneticFunction(solutions, pilots, N, mutationProbability, selectionPercentage, ift, numberOfChromosomes, totalSPs, overlapArr,spIdOverlapArr, elevenHoursRestArr, earlierDateGiven, latestDateGiven, flightDays, crossoverMethodGiven, SPList)
	
	if bestSolution != None:
		print "best fitness func:", smallest
		print "best solution:", bestSolution

		for j in xrange(0,N):

			print "pilots", j, "real time:", pilotsbest[j].FTpilot
	else:
		print "There is no solution to this problem"

	#****------------------------****#

	#****-------------------------****#

	# Rules.printPenalty(penalty,numberOfChromosomes)

	# Rules.printRule1Array(pilots)

	# Functions.printSP(SPList)

	# Functions.printPilotsList(pilots, N, numberOfChromosomes)

	# Functions.printSolutionsList(solutions, numberOfChromosomes, totalSPs,SPList)

	#****-------------------------****#


if __name__ == "__main__":
   main(sys.argv[1:])




# SPList, pilots, solutions
