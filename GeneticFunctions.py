import math
import sys
import datetime as dt
from argparse import ArgumentParser
import Structures
import Functions
import random
import Rules

def Selection(solutions,selectedSolutions, selectionPercentage):
	solutions = sorted(solutions, key=lambda solution: solution.fitness,reverse=True)
	selectedSolutions = solutions[:int(selectionPercentage * len(solutions))]
	return solutions, selectedSolutions

def updateWeakest(solutions,selectedSolutions, selectionPercentage, totalSPs):
	for solNum in xrange(0, len(selectedSolutions)):

		for sp in xrange(0, totalSPs):
			solutions[len(solutions) - solNum - 1].solutionArray[sp] = selectedSolutions[solNum].solutionArray[sp] #h arithmhsh tou pinaka solutions einai mexri len(solutions) -1
		solutions[len(solutions) - solNum - 1].fitness = selectedSolutions[solNum].fitness
	return solutions


def singlePointCrossover(selectedSolutions, totalSPs):
	
	offsprings = []

	if (len(selectedSolutions) % 2) != 0:
		selectedSolutions.pop()
	
	pickedsplitPoint = random.randint(0,totalSPs-1)
	print "singlePointCrossover", pickedsplitPoint

	while True:
		if len(selectedSolutions) == 0:
			break

		parent1 = random.choice(selectedSolutions)

		while True:
			parent2 = random.choice(selectedSolutions)
			if parent2 != parent1:
				break

		child1 = Structures.Solution(0,[],0.0)
		child2 = Structures.Solution(0,[],0.0)

		child1.solutionArray = parent1.solutionArray[0:pickedsplitPoint] + parent2.solutionArray[pickedsplitPoint:len(parent2.solutionArray)]
		child2.solutionArray = parent2.solutionArray[0:pickedsplitPoint] + parent1.solutionArray[pickedsplitPoint:len(parent1.solutionArray)]
		
		offsprings.append(child1)
		offsprings.append(child2)


		selectedSolutions.remove(parent1)
		selectedSolutions.remove(parent2)

	return offsprings
	

def twoPointCrossover(selectedSolutions, totalSPs):
	print "twoPointCrossover"

	offsprings = []

	if (len(selectedSolutions) % 2) != 0:
		selectedSolutions.pop()

	pickedsplitPoint1 = random.randint(0,len(selectedSolutions)-1)
	pickedsplitPoint2 = random.randint(pickedsplitPoint1,len(selectedSolutions)-1)

	while True:
		if len(selectedSolutions) == 0:
			break

		parent1 = random.choice(selectedSolutions)

		while True:
			parent2 = random.choice(selectedSolutions)
			if parent2 != parent1:
				break

		child1 = Structures.Solution(0,[],0.0)
		child2 = Structures.Solution(0,[],0.0)

		child1.solutionArray = parent1.solutionArray[0:pickedsplitPoint1] + parent2.solutionArray[pickedsplitPoint1:pickedsplitPoint2] + parent1.solutionArray[pickedsplitPoint2:len(parent1.solutionArray)]
		child2.solutionArray = parent2.solutionArray[0:pickedsplitPoint1] + parent1.solutionArray[pickedsplitPoint1:pickedsplitPoint2] + parent2.solutionArray[pickedsplitPoint2:len(parent2.solutionArray)]
		
		
		offsprings.append(child1)
		offsprings.append(child2)


		selectedSolutions.remove(parent1)
		selectedSolutions.remove(parent2)

	return offsprings


def uniformCrossover(selectedSolutions, totalSPs):
	print "uniformCrossover"

	if (len(selectedSolutions) % 2) != 0:
		selectedSolutions.pop()



def mutation(offsprings, mutationProbability, totalSPs, N):
	# print "mutation"

	
	numOfMutated = (mutationProbability * totalSPs) + int(0.008* totalSPs)
	# numOfMutated =  int(0.05* totalSPs)
	# print int(numOfMutated)

	for x in xrange(1,int(numOfMutated) ):
		toBeMutated = random.randint(0,totalSPs-1)
		# print x  prints the number of mutated gens

		for offspring in offsprings:
			offspring.solutionArray[toBeMutated] = random.randint(1,N)

	return offsprings


def fitnessFunction(pilots, IFT, solutionNum, penalty):
	V = 0

	for pilot in pilots[solutionNum]:
		# print pilot.pilotId
		V += (pilot.FTpilot - IFT)**2

	if penalty == 1000:
		V = V**4    #an parabiazetai kapoios kanonas tote auksanw para polu to V wste h lush auth na aporriuthei
	
	# print "fitnessFunction", V
	return 100000/V


def IFT(SPList, totalSPs, N):
	ftpSumMin = 0

	for sp in SPList:
		ftpSumMin += float(sp.flightSPDuration.seconds)/float(3600)  #o upologismos ginetai se wres
	
	ift = ftpSumMin/N                                 #upologismos se wres
	print "IFT:", ift
	return ift


def FTP(SPList):  #upologizetai hdh sthn lista SPList ws flightSPDuration
	for sp in SPList:
		print "FTP:", sp.spId, float(sp.flightSPDuration.seconds)/float(3600)


def FT(pilots, SPList):  #uparxei hdh sto struct tou pilotou ws FTpilot kai upologizetai kathe fora p anatithontai sps ston piloto
	print "ft"


def geneticFunction(solutions, pilots, N, mutationProbability, selectionPercentage, IFT, numberOfChromosomes, totalSPs, overlapArr,spIdOverlapArr, elevenHoursRestArr, earlierDateGiven, latestDateGiven, flightDays, crossoverMethodGiven, SPList):
	print "geneticFunction"
	i = 0
	genOfSmallest = 0

	penalty = [0] * numberOfChromosomes
	penalty = Rules.rulesCheck(penalty, solutions, pilots, numberOfChromosomes, totalSPs, overlapArr,spIdOverlapArr, elevenHoursRestArr)
	penalty = Rules.rule1Check(penalty, solutions, pilots, numberOfChromosomes, earlierDateGiven, latestDateGiven, flightDays)

	smallest = 900000000.0
	bestSolution = None

	for sol in xrange(0,len(solutions)):
		solutions[sol].fitness = fitnessFunction(pilots, IFT, sol, penalty[sol])
		
		if (solutions[sol].fitness**(-1))*100000 < smallest:
			smallest = (solutions[sol].fitness**(-1))*100000
			print "smallest:", smallest
			genOfSmallest = i


	while True:
		# if (i - genOfSmallest >= 1000) and penalty[:] != 1000:
		if i - genOfSmallest >= 600:
			break
	
		# Functions.printPilotsList(pilots, N, numberOfChromosomes)

		print "\ngeneration", i
		i += 1


		selectedSolutions = []
		solutions, selectedSolutions = Selection(solutions, selectedSolutions, selectionPercentage)


		offsprings = []
		
		if crossoverMethodGiven is 1:
				offsprings = singlePointCrossover(selectedSolutions, totalSPs)
		elif crossoverMethodGiven is 2:
				offsprings = twoPointCrossover(selectedSolutions, totalSPs)
		elif crossoverMethodGiven is 3:
				offsprings = uniformCrossover(selectedSolutions, totalSPs)
		
		offsprings = mutation(offsprings, mutationProbability, totalSPs, N)

		solutions = updateWeakest(solutions,offsprings, selectionPercentage, totalSPs)
		
		#prepei se kathe epanalhpsh na katharizw th lista twn pilotwn me ta sp prin thn ksanagemisw
		pilots = Functions.clearPilotsList(pilots, flightDays) #meta apo kathe gemisma twn solutions arxikopoioume ton pinaka tou kanona 1

		pilots = Functions.setSPToPilots(solutions, pilots, N, numberOfChromosomes, totalSPs, SPList)


		penalty = [0] * numberOfChromosomes
		penalty = Rules.rulesCheck(penalty, solutions, pilots, numberOfChromosomes, totalSPs, overlapArr,spIdOverlapArr, elevenHoursRestArr)
		penalty = Rules.rule1Check(penalty, solutions, pilots, numberOfChromosomes, earlierDateGiven, latestDateGiven, flightDays)

		# Rules.printPenalty(penalty,numberOfChromosomes)
		stop = 0
		thereIsPenalty = 0
		for sol in xrange(0,len(solutions)):
			solutions[sol].fitness = fitnessFunction(pilots, IFT, sol, penalty[sol])

			if (solutions[sol].fitness**(-1))*100000 < smallest:

				# **********************************************************
				if ((((solutions[sol].fitness**(-1))*100000)/smallest)-1) > -0.01:  #edw koitaw an duo diaforetikes veltistes times exoun diafora mikroterh tou 1% tote stamataw
					stop = 1
				# **********************************************************
				smallest = (solutions[sol].fitness**(-1))*100000
				bestSolution = solutions[sol].solutionArray
				genOfSmallest = i
				pilotsBest = pilots[0]

				if penalty[sol] == 1000:
					thereIsPenalty = 1
				else:
					thereIsPenalty = 0
					
				print "smallest:", smallest
				print "penalty:", thereIsPenalty
				print "best solution:", bestSolution

				# ************************************
		if stop == 1:
			break
				# ************************************

	return smallest, bestSolution, pilotsBest







#to FTP kathe sp upologizetai mia fora sthn arxh gia tous dothentes sp. uparxei ws flightSPDuration sto struct tou sp
#to IFT einai o idanikos xronos pthshs. upologizetai vash tous FTP. ara upologizetai mia fora sthn arxh
#to FT einai o sunolikos xronos pthshs kathe pilotou kai upologizetai meta apo kathe anathesh. uparxei hdh sto struct tou pilotou
#to selection ginetai me vash tous k kaluterous individuals. afou ginetai to reproduction 
#ginetai to elitism. ta offsprings antikathistoun tous xeiroterous individuals.
#ws break point apofasizw oti tha ginoun to polu 1000 genies akoma apo thn teleutaia fora
	#p vrethhke smallest