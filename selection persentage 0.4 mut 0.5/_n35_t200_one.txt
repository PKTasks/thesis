Namespace(crossoverMethod=None, endtime='2011-12-14/23:59', filename='pairings.txt', n=[40], numberOfChromosomes=200, output=None, seed=None, starttime='2011-12-01/00:01')
startTimeGiven is: 2011-12-01 00:01:00
endTimeGiven is: 2011-12-14 23:59:00
seed is: None
outputFile is: None
crossover method: None
N = 40
totalSPs: 382 

mutation : 0.00261780104712
IFT: 44.3833333333
geneticFunction

generation 0
singlePointCrossover 296

generation 1
singlePointCrossover 171

generation 2
singlePointCrossover 378

generation 3
singlePointCrossover 237

generation 4
singlePointCrossover 31

generation 5
singlePointCrossover 234

generation 6
singlePointCrossover 166

generation 7
singlePointCrossover 227

generation 8
singlePointCrossover 11

generation 9
singlePointCrossover 163

generation 10
singlePointCrossover 63

generation 11
singlePointCrossover 58

generation 12
singlePointCrossover 66

generation 13
singlePointCrossover 316

generation 14
singlePointCrossover 73

generation 15
singlePointCrossover 259

generation 16
singlePointCrossover 31

generation 17
singlePointCrossover 4

generation 18
singlePointCrossover 75

generation 19
singlePointCrossover 329

generation 20
singlePointCrossover 184

generation 21
singlePointCrossover 10

generation 22
singlePointCrossover 230

generation 23
singlePointCrossover 112

generation 24
singlePointCrossover 162

generation 25
singlePointCrossover 208

generation 26
singlePointCrossover 38

generation 27
singlePointCrossover 79

generation 28
singlePointCrossover 7

generation 29
singlePointCrossover 359

generation 30
singlePointCrossover 43

generation 31
singlePointCrossover 283

generation 32
singlePointCrossover 33

generation 33
singlePointCrossover 349

generation 34
singlePointCrossover 261

generation 35
singlePointCrossover 84

generation 36
singlePointCrossover 1

generation 37
singlePointCrossover 124

generation 38
singlePointCrossover 301

generation 39
singlePointCrossover 28

generation 40
singlePointCrossover 342

generation 41
singlePointCrossover 230

generation 42
singlePointCrossover 247

generation 43
singlePointCrossover 286

generation 44
singlePointCrossover 236

generation 45
singlePointCrossover 365

generation 46
singlePointCrossover 121

generation 47
singlePointCrossover 348

generation 48
singlePointCrossover 142

generation 49
singlePointCrossover 329

generation 50
singlePointCrossover 345

generation 51
singlePointCrossover 349

generation 52
singlePointCrossover 91

generation 53
singlePointCrossover 287

generation 54
singlePointCrossover 195

generation 55
singlePointCrossover 311

generation 56
singlePointCrossover 348

generation 57
singlePointCrossover 186

generation 58
singlePointCrossover 323

generation 59
singlePointCrossover 259

generation 60
singlePointCrossover 235

generation 61
singlePointCrossover 302

generation 62
singlePointCrossover 82

generation 63
singlePointCrossover 230

generation 64
singlePointCrossover 232

generation 65
singlePointCrossover 57

generation 66
singlePointCrossover 164

generation 67
singlePointCrossover 126

generation 68
singlePointCrossover 275

generation 69
singlePointCrossover 105

generation 70
singlePointCrossover 347

generation 71
singlePointCrossover 158

generation 72
singlePointCrossover 220

generation 73
singlePointCrossover 339

generation 74
singlePointCrossover 28

generation 75
singlePointCrossover 241

generation 76
singlePointCrossover 80

generation 77
singlePointCrossover 102

generation 78
singlePointCrossover 87

generation 79
singlePointCrossover 184

generation 80
singlePointCrossover 170

generation 81
singlePointCrossover 240

generation 82
singlePointCrossover 328

generation 83
singlePointCrossover 238

generation 84
singlePointCrossover 337

generation 85
singlePointCrossover 381

generation 86
singlePointCrossover 313

generation 87
singlePointCrossover 263

generation 88
singlePointCrossover 263

generation 89
singlePointCrossover 75

generation 90
singlePointCrossover 29

generation 91
singlePointCrossover 83

generation 92
singlePointCrossover 112

generation 93
singlePointCrossover 349

generation 94
singlePointCrossover 83

generation 95
singlePointCrossover 365

generation 96
singlePointCrossover 21

generation 97
singlePointCrossover 324

generation 98
singlePointCrossover 281

generation 99
singlePointCrossover 272

generation 100
singlePointCrossover 266

generation 101
singlePointCrossover 362

generation 102
singlePointCrossover 369

generation 103
singlePointCrossover 276

generation 104
singlePointCrossover 206

generation 105
singlePointCrossover 380

generation 106
singlePointCrossover 300

generation 107
singlePointCrossover 45

generation 108
singlePointCrossover 338

generation 109
singlePointCrossover 240

generation 110
singlePointCrossover 367

generation 111
singlePointCrossover 37

generation 112
singlePointCrossover 152

generation 113
singlePointCrossover 300

generation 114
singlePointCrossover 351

generation 115
singlePointCrossover 144

generation 116
singlePointCrossover 118

generation 117
singlePointCrossover 43

generation 118
singlePointCrossover 38

generation 119
singlePointCrossover 262

generation 120
singlePointCrossover 342

generation 121
singlePointCrossover 269

generation 122
singlePointCrossover 290

generation 123
singlePointCrossover 360

generation 124
singlePointCrossover 176

generation 125
singlePointCrossover 230

generation 126
singlePointCrossover 132

generation 127
singlePointCrossover 238

generation 128
singlePointCrossover 102

generation 129
singlePointCrossover 108

generation 130
singlePointCrossover 55

generation 131
singlePointCrossover 177

generation 132
singlePointCrossover 358

generation 133
singlePointCrossover 173

generation 134
singlePointCrossover 103

generation 135
singlePointCrossover 159

generation 136
singlePointCrossover 236

generation 137
singlePointCrossover 356

generation 138
singlePointCrossover 169

generation 139
singlePointCrossover 333

generation 140
singlePointCrossover 197

generation 141
singlePointCrossover 276

generation 142
singlePointCrossover 310

generation 143
singlePointCrossover 227

generation 144
singlePointCrossover 381

generation 145
singlePointCrossover 380

generation 146
singlePointCrossover 362

generation 147
singlePointCrossover 297

generation 148
singlePointCrossover 361

generation 149
singlePointCrossover 215

generation 150
singlePointCrossover 304

generation 151
singlePointCrossover 58

generation 152
singlePointCrossover 191

generation 153
singlePointCrossover 256

generation 154
singlePointCrossover 255

generation 155
singlePointCrossover 38

generation 156
singlePointCrossover 91

generation 157
singlePointCrossover 3

generation 158
singlePointCrossover 343

generation 159
singlePointCrossover 75

generation 160
singlePointCrossover 346

generation 161
singlePointCrossover 364

generation 162
singlePointCrossover 166

generation 163
singlePointCrossover 184

generation 164
singlePointCrossover 287

generation 165
singlePointCrossover 329

generation 166
singlePointCrossover 57

generation 167
singlePointCrossover 329

generation 168
singlePointCrossover 142

generation 169
singlePointCrossover 293

generation 170
singlePointCrossover 209

generation 171
singlePointCrossover 85

generation 172
singlePointCrossover 187

generation 173
singlePointCrossover 213

generation 174
singlePointCrossover 257

generation 175
singlePointCrossover 276

generation 176
singlePointCrossover 168

generation 177
singlePointCrossover 354

generation 178
singlePointCrossover 59

generation 179
singlePointCrossover 178

generation 180
singlePointCrossover 194

generation 181
singlePointCrossover 10

generation 182
singlePointCrossover 365

generation 183
singlePointCrossover 275

generation 184
singlePointCrossover 301

generation 185
singlePointCrossover 124

generation 186
singlePointCrossover 378

generation 187
singlePointCrossover 346

generation 188
singlePointCrossover 363

generation 189
singlePointCrossover 8

generation 190
singlePointCrossover 334

generation 191
singlePointCrossover 146

generation 192
singlePointCrossover 334

generation 193
singlePointCrossover 67

generation 194
singlePointCrossover 311

generation 195
singlePointCrossover 344

generation 196
singlePointCrossover 225

generation 197
singlePointCrossover 192

generation 198
singlePointCrossover 105

generation 199
singlePointCrossover 326

generation 200
singlePointCrossover 102

generation 201
singlePointCrossover 86

generation 202
singlePointCrossover 31

generation 203
singlePointCrossover 340

generation 204
singlePointCrossover 373

generation 205
singlePointCrossover 50

generation 206
singlePointCrossover 322

generation 207
singlePointCrossover 203

generation 208
singlePointCrossover 115

generation 209
singlePointCrossover 321

generation 210
singlePointCrossover 159

generation 211
singlePointCrossover 246

generation 212
singlePointCrossover 300

generation 213
singlePointCrossover 35

generation 214
singlePointCrossover 169

generation 215
singlePointCrossover 100

generation 216
singlePointCrossover 229

generation 217
singlePointCrossover 13

generation 218
singlePointCrossover 111

generation 219
singlePointCrossover 300

generation 220
singlePointCrossover 15

generation 221
singlePointCrossover 180

generation 222
singlePointCrossover 171

generation 223
singlePointCrossover 201

generation 224
singlePointCrossover 18

generation 225
singlePointCrossover 189

generation 226
singlePointCrossover 276

generation 227
singlePointCrossover 295

generation 228
singlePointCrossover 200

generation 229
singlePointCrossover 145

generation 230
singlePointCrossover 334

generation 231
singlePointCrossover 121

generation 232
singlePointCrossover 116

generation 233
singlePointCrossover 322

generation 234
singlePointCrossover 271

generation 235
singlePointCrossover 346

generation 236
singlePointCrossover 326

generation 237
singlePointCrossover 102

generation 238
singlePointCrossover 237

generation 239
singlePointCrossover 141

generation 240
singlePointCrossover 16

generation 241
singlePointCrossover 102

generation 242
singlePointCrossover 178

generation 243
singlePointCrossover 61

generation 244
singlePointCrossover 286

generation 245
singlePointCrossover 354

generation 246
singlePointCrossover 160

generation 247
singlePointCrossover 258

generation 248
singlePointCrossover 294

generation 249
singlePointCrossover 160

generation 250
singlePointCrossover 195

generation 251
singlePointCrossover 67

generation 252
singlePointCrossover 289

generation 253
singlePointCrossover 329

generation 254
singlePointCrossover 307

generation 255
singlePointCrossover 334

generation 256
singlePointCrossover 65

generation 257
singlePointCrossover 127

generation 258
singlePointCrossover 56

generation 259
singlePointCrossover 29

generation 260
singlePointCrossover 41

generation 261
singlePointCrossover 39

generation 262
singlePointCrossover 365

generation 263
singlePointCrossover 64

generation 264
singlePointCrossover 214

generation 265
singlePointCrossover 193

generation 266
singlePointCrossover 208

generation 267
singlePointCrossover 229

generation 268
singlePointCrossover 335

generation 269
singlePointCrossover 328

generation 270
singlePointCrossover 201

generation 271
singlePointCrossover 274

generation 272
singlePointCrossover 369

generation 273
singlePointCrossover 354

generation 274
singlePointCrossover 199

generation 275
singlePointCrossover 375

generation 276
singlePointCrossover 83

generation 277
singlePointCrossover 59

generation 278
singlePointCrossover 291

generation 279
singlePointCrossover 105

generation 280
singlePointCrossover 248

generation 281
singlePointCrossover 362

generation 282
singlePointCrossover 315

generation 283
singlePointCrossover 312

generation 284
singlePointCrossover 225

generation 285
singlePointCrossover 192

generation 286
singlePointCrossover 373

generation 287
singlePointCrossover 263

generation 288
singlePointCrossover 298

generation 289
singlePointCrossover 73

generation 290
singlePointCrossover 116

generation 291
singlePointCrossover 208

generation 292
singlePointCrossover 242

generation 293
singlePointCrossover 52

generation 294
singlePointCrossover 381

generation 295
singlePointCrossover 290

generation 296
singlePointCrossover 96

generation 297
singlePointCrossover 197

generation 298
singlePointCrossover 258

generation 299
singlePointCrossover 5

generation 300
singlePointCrossover 206

generation 301
singlePointCrossover 19

generation 302
singlePointCrossover 100

generation 303
singlePointCrossover 107

generation 304
singlePointCrossover 118

generation 305
singlePointCrossover 88

generation 306
singlePointCrossover 301

generation 307
singlePointCrossover 319

generation 308
singlePointCrossover 366

generation 309
singlePointCrossover 211

generation 310
singlePointCrossover 51

generation 311
singlePointCrossover 362

generation 312
singlePointCrossover 80

generation 313
singlePointCrossover 251

generation 314
singlePointCrossover 7

generation 315
singlePointCrossover 30

generation 316
singlePointCrossover 359

generation 317
singlePointCrossover 187

generation 318
singlePointCrossover 80

generation 319
singlePointCrossover 67

generation 320
singlePointCrossover 71

generation 321
singlePointCrossover 345

generation 322
singlePointCrossover 284

generation 323
singlePointCrossover 27

generation 324
singlePointCrossover 214

generation 325
singlePointCrossover 91

generation 326
singlePointCrossover 95

generation 327
singlePointCrossover 333

generation 328
singlePointCrossover 356

generation 329
singlePointCrossover 336

generation 330
singlePointCrossover 116

generation 331
singlePointCrossover 271

generation 332
singlePointCrossover 351

generation 333
singlePointCrossover 26

generation 334
singlePointCrossover 21

generation 335
singlePointCrossover 216

generation 336
singlePointCrossover 118

generation 337
singlePointCrossover 191

generation 338
singlePointCrossover 168

generation 339
singlePointCrossover 295

generation 340
singlePointCrossover 58

generation 341
singlePointCrossover 48

generation 342
singlePointCrossover 324

generation 343
singlePointCrossover 165

generation 344
singlePointCrossover 41

generation 345
singlePointCrossover 116

generation 346
singlePointCrossover 139

generation 347
singlePointCrossover 247

generation 348
singlePointCrossover 276

generation 349
singlePointCrossover 76

generation 350
singlePointCrossover 217

generation 351
singlePointCrossover 300

generation 352
singlePointCrossover 325

generation 353
singlePointCrossover 1

generation 354
singlePointCrossover 73

generation 355
singlePointCrossover 129

generation 356
singlePointCrossover 246

generation 357
singlePointCrossover 309

generation 358
singlePointCrossover 201

generation 359
singlePointCrossover 314

generation 360
singlePointCrossover 147

generation 361
singlePointCrossover 245

generation 362
singlePointCrossover 179

generation 363
singlePointCrossover 358

generation 364
singlePointCrossover 83

generation 365
singlePointCrossover 298

generation 366
singlePointCrossover 275

generation 367
singlePointCrossover 179

generation 368
singlePointCrossover 359

generation 369
singlePointCrossover 14

generation 370
singlePointCrossover 313

generation 371
singlePointCrossover 147

generation 372
singlePointCrossover 184

generation 373
singlePointCrossover 170

generation 374
singlePointCrossover 209

generation 375
singlePointCrossover 152

generation 376
singlePointCrossover 177

generation 377
singlePointCrossover 353

generation 378
singlePointCrossover 294

generation 379
singlePointCrossover 10

generation 380
singlePointCrossover 179

generation 381
singlePointCrossover 16

generation 382
singlePointCrossover 212

generation 383
singlePointCrossover 250

generation 384
singlePointCrossover 234

generation 385
singlePointCrossover 179

generation 386
singlePointCrossover 314

generation 387
singlePointCrossover 336

generation 388
singlePointCrossover 174

generation 389
singlePointCrossover 128

generation 390
singlePointCrossover 377

generation 391
singlePointCrossover 77

generation 392
singlePointCrossover 131

generation 393
singlePointCrossover 288

generation 394
singlePointCrossover 282

generation 395
singlePointCrossover 70

generation 396
singlePointCrossover 7

generation 397
singlePointCrossover 329

generation 398
singlePointCrossover 130

generation 399
singlePointCrossover 374

generation 400
singlePointCrossover 252

generation 401
singlePointCrossover 258

generation 402
singlePointCrossover 319

generation 403
singlePointCrossover 262

generation 404
singlePointCrossover 281

generation 405
singlePointCrossover 60

generation 406
singlePointCrossover 131

generation 407
singlePointCrossover 199

generation 408
singlePointCrossover 233

generation 409
singlePointCrossover 326

generation 410
singlePointCrossover 35

generation 411
singlePointCrossover 140

generation 412
singlePointCrossover 358

generation 413
singlePointCrossover 131

generation 414
singlePointCrossover 117

generation 415
singlePointCrossover 306

generation 416
singlePointCrossover 211

generation 417
singlePointCrossover 188

generation 418
singlePointCrossover 212

generation 419
singlePointCrossover 168

generation 420
singlePointCrossover 71

generation 421
singlePointCrossover 317

generation 422
singlePointCrossover 230

generation 423
singlePointCrossover 28

generation 424
singlePointCrossover 106

generation 425
singlePointCrossover 246

generation 426
singlePointCrossover 68

generation 427
singlePointCrossover 69

generation 428
singlePointCrossover 183

generation 429
singlePointCrossover 151

generation 430
singlePointCrossover 313

generation 431
singlePointCrossover 173

generation 432
singlePointCrossover 66

generation 433
singlePointCrossover 363

generation 434
singlePointCrossover 270

generation 435
singlePointCrossover 243

generation 436
singlePointCrossover 195

generation 437
singlePointCrossover 127

generation 438
singlePointCrossover 213

generation 439
singlePointCrossover 238

generation 440
singlePointCrossover 49

generation 441
singlePointCrossover 324

generation 442
singlePointCrossover 281

generation 443
singlePointCrossover 159

generation 444
singlePointCrossover 182

generation 445
singlePointCrossover 320

generation 446
singlePointCrossover 203

generation 447
singlePointCrossover 72

generation 448
singlePointCrossover 164

generation 449
singlePointCrossover 76

generation 450
singlePointCrossover 158

generation 451
singlePointCrossover 240

generation 452
singlePointCrossover 323

generation 453
singlePointCrossover 167

generation 454
singlePointCrossover 155

generation 455
singlePointCrossover 172

generation 456
singlePointCrossover 275

generation 457
singlePointCrossover 123

generation 458
singlePointCrossover 159

generation 459
singlePointCrossover 292

generation 460
singlePointCrossover 262

generation 461
singlePointCrossover 87

generation 462
singlePointCrossover 22

generation 463
singlePointCrossover 72

generation 464
singlePointCrossover 74

generation 465
singlePointCrossover 337

generation 466
singlePointCrossover 99

generation 467
singlePointCrossover 370

generation 468
singlePointCrossover 261

generation 469
singlePointCrossover 315

generation 470
singlePointCrossover 43

generation 471
singlePointCrossover 135

generation 472
singlePointCrossover 46

generation 473
singlePointCrossover 364

generation 474
singlePointCrossover 56

generation 475
singlePointCrossover 229

generation 476
singlePointCrossover 9

generation 477
singlePointCrossover 303

generation 478
singlePointCrossover 137

generation 479
singlePointCrossover 93

generation 480
singlePointCrossover 199

generation 481
singlePointCrossover 221

generation 482
singlePointCrossover 211

generation 483
singlePointCrossover 204

generation 484
singlePointCrossover 130

generation 485
singlePointCrossover 268

generation 486
singlePointCrossover 8

generation 487
singlePointCrossover 56

generation 488
singlePointCrossover 32

generation 489
singlePointCrossover 278

generation 490
singlePointCrossover 333

generation 491
singlePointCrossover 26

generation 492
singlePointCrossover 266

generation 493
singlePointCrossover 317

generation 494
singlePointCrossover 335

generation 495
singlePointCrossover 41

generation 496
singlePointCrossover 225

generation 497
singlePointCrossover 146

generation 498
singlePointCrossover 174

generation 499
singlePointCrossover 12

generation 500
singlePointCrossover 108

generation 501
singlePointCrossover 259

generation 502
singlePointCrossover 250

generation 503
singlePointCrossover 293

generation 504
singlePointCrossover 105

generation 505
singlePointCrossover 72

generation 506
singlePointCrossover 91

generation 507
singlePointCrossover 258

generation 508
singlePointCrossover 381

generation 509
singlePointCrossover 312

generation 510
singlePointCrossover 256

generation 511
singlePointCrossover 320

generation 512
singlePointCrossover 39

generation 513
singlePointCrossover 47

generation 514
singlePointCrossover 148

generation 515
singlePointCrossover 151

generation 516
singlePointCrossover 230

generation 517
singlePointCrossover 346

generation 518
singlePointCrossover 336

generation 519
singlePointCrossover 37

generation 520
singlePointCrossover 161

generation 521
singlePointCrossover 239

generation 522
singlePointCrossover 226

generation 523
singlePointCrossover 116

generation 524
singlePointCrossover 18

generation 525
singlePointCrossover 233

generation 526
singlePointCrossover 346

generation 527
singlePointCrossover 346

generation 528
singlePointCrossover 215

generation 529
singlePointCrossover 84

generation 530
singlePointCrossover 166

generation 531
singlePointCrossover 141

generation 532
singlePointCrossover 22

generation 533
singlePointCrossover 17

generation 534
singlePointCrossover 310

generation 535
singlePointCrossover 232

generation 536
singlePointCrossover 222

generation 537
singlePointCrossover 307

generation 538
singlePointCrossover 75

generation 539
singlePointCrossover 330

generation 540
singlePointCrossover 332

generation 541
singlePointCrossover 245

generation 542
singlePointCrossover 155

generation 543
singlePointCrossover 72

generation 544
singlePointCrossover 14

generation 545
singlePointCrossover 338

generation 546
singlePointCrossover 337

generation 547
singlePointCrossover 349

generation 548
singlePointCrossover 172

generation 549
singlePointCrossover 256

generation 550
singlePointCrossover 247

generation 551
singlePointCrossover 63

generation 552
singlePointCrossover 337

generation 553
singlePointCrossover 363

generation 554
singlePointCrossover 339

generation 555
singlePointCrossover 145

generation 556
singlePointCrossover 106

generation 557
singlePointCrossover 170

generation 558
singlePointCrossover 26

generation 559
singlePointCrossover 251

generation 560
singlePointCrossover 59

generation 561
singlePointCrossover 2

generation 562
singlePointCrossover 0

generation 563
singlePointCrossover 365

generation 564
singlePointCrossover 295

generation 565
singlePointCrossover 334

generation 566
singlePointCrossover 284

generation 567
singlePointCrossover 231

generation 568
singlePointCrossover 332

generation 569
singlePointCrossover 42

generation 570
singlePointCrossover 23

generation 571
singlePointCrossover 227

generation 572
singlePointCrossover 140

generation 573
singlePointCrossover 102

generation 574
singlePointCrossover 377

generation 575
singlePointCrossover 63

generation 576
singlePointCrossover 71

generation 577
singlePointCrossover 73

generation 578
singlePointCrossover 162

generation 579
singlePointCrossover 14

generation 580
singlePointCrossover 199

generation 581
singlePointCrossover 334

generation 582
singlePointCrossover 312

generation 583
singlePointCrossover 57

generation 584
singlePointCrossover 277

generation 585
singlePointCrossover 205

generation 586
singlePointCrossover 23

generation 587
singlePointCrossover 138

generation 588
singlePointCrossover 84

generation 589
singlePointCrossover 118

generation 590
singlePointCrossover 40

generation 591
singlePointCrossover 104

generation 592
singlePointCrossover 77

generation 593
singlePointCrossover 299

generation 594
singlePointCrossover 250

generation 595
singlePointCrossover 356

generation 596
singlePointCrossover 9

generation 597
singlePointCrossover 57

generation 598
singlePointCrossover 28

generation 599
singlePointCrossover 340
There is no solution to this problem
