#Functions.py
import math
import sys
import datetime as dt
from argparse import ArgumentParser
import GeneticFunctions
import Structures
import time
import random

def ReadFromFile(fileName):
	try:
		with open(fileName, "r") as f:
			lines = [line.strip() for line in f if line.strip()]
		# print r
	except:
		print "error: [cant open the file to read]"
		exit(5)

	return lines


def ReadFromPrompt():
	print "Enter:"
	x = raw_input()
	return x




def findTotalSP(dataFromFile, totalSPs, prevSpId, startTimeGiven, endTimeGiven): #vriskw to sunolo twn SP
	for line in dataFromFile:    #kanw parse to arxeio kai vriskw xronous anaxwrhshs kai afikshs
		try:
			lineSplit = line.split(' ')
			# print lineSplit
			# print lineSplit[4] + " " + lineSplit[5]
			startTime = lineSplit[4] + " " + lineSplit[5]
			startTime = dt.datetime.strptime(startTime, '%Y-%m-%d %H:%M')
		except:
			print "error 1"
			exit()

		try:
			if lineSplit[0] != prevSpId:
				if(startTime >= startTimeGiven and startTime <= endTimeGiven):
					totalSPs += 1
			prevSpId = lineSplit[0]
			# print "\n"
		except:
			print "error: [cant compute total SPs]"
			exit(10)
	return totalSPs


def keyFunc(e):
	return e.spId


def spCreateList(dataFromFile,startTimeGiven,endTimeGiven):
	spList = []
	isAccepted = 0
	prevSpId = "0000"

	earlierDateGiven = dt.datetime.strptime("9999-01-01 00:00", '%Y-%m-%d %H:%M')
	latestDateGiven = dt.datetime.strptime("0001-01-01 00:00", '%Y-%m-%d %H:%M')

	for line in dataFromFile:    #kanw parse to arxeio kai vriskw xronous anaxwrhshs kai afikshs
		try:
			# print line
			lineSplit = line.split(' ')
			dateTime = lineSplit[4] + " " + lineSplit[5]
			depDateTime = dt.datetime.strptime(dateTime, '%Y-%m-%d %H:%M')

			if depDateTime < earlierDateGiven:
				earlierDateGiven = dt.datetime.strptime(dateTime, '%Y-%m-%d %H:%M')

			dateTime = lineSplit[6] + " " + lineSplit[7]
			arrDateTime = dt.datetime.strptime(dateTime, '%Y-%m-%d %H:%M')

			if latestDateGiven < arrDateTime:
				latestDateGiven = dt.datetime.strptime(dateTime, '%Y-%m-%d %H:%M')

		except:
			print "error spCreateList"
			exit()


		if lineSplit[0] != prevSpId:
			if(depDateTime >= startTimeGiven and depDateTime <= endTimeGiven):
				SP = Structures.SP(0,[],0)
				SP.spId = lineSplit[0]          #int(lineSplit[0])
				SP.flightSPDuration = 0
				spList.append(SP)
				isAccepted = 1
			else:
				isAccepted = 0

		if isAccepted == 1:
			flight = flightInit(lineSplit[1],lineSplit[2],lineSplit[3],depDateTime,arrDateTime)
			spList[-1].flightArray.append(flight)    #paei sto teleutaio keli ths listas


		prevSpId = lineSplit[0]

	for k in spList:                            #upologizoume to flightSPDUration
		k.flightSPDuration = dt.datetime.strptime("0001-01-01 00:00", '%Y-%m-%d %H:%M')
		numOfFlights = 0

		for i in k.flightArray:
			numOfFlights += 1
			k.flightSPDuration = k.flightSPDuration + i.flightDuration

		k.flightSPDuration = dt.timedelta(hours=k.flightSPDuration.hour,minutes=k.flightSPDuration.minute,seconds=k.flightSPDuration.second).total_seconds()
		k.flightSPDuration = dt.timedelta(seconds = k.flightSPDuration)
		# print k.flightSPDuration
		k.numOfFlights = numOfFlights
		# print k.spId, k.numOfFlights

	spList.sort(key=keyFunc)

	# print earlierDateGiven
	# print latestDateGiven
	return spList, earlierDateGiven, latestDateGiven



def pilotsCreateList(N, numberOfChromosomes, earlierDateGiven, latestDateGiven):
	pilots = [None]*numberOfChromosomes

	flightDays = totalFlightDays(earlierDateGiven, latestDateGiven)  #total days of flight of the airline
	# print flightDays

	for k in xrange(0,len(pilots)):
		pilots[k] = pilotsAllocate(N, flightDays)  #desmeuw mia lista me tous pilotous
	return pilots


def pilotsAllocate(N, flightDays):
	pilots = []

	for x in xrange(1,N + 1):
		pilot = Structures.Pilot(0,[],[],0)

		pilot.rule1Array = [0] * flightDays
		# print pilot.rule1Array

		pilots.append(pilot)
		pilots[x - 1].pilotId = x

	return pilots




def totalFlightDays(earlierDateGiven, latestDateGiven):
	# print earlierDateGiven
	# print latestDateGiven
	# print (latestDateGiven - earlierDateGiven).days

	return (latestDateGiven - earlierDateGiven).days + 1 # +1 gt logw afaireshs xanetai mia mera



def flightInit(flightId,depAirport,arrAirport,depDatetime,arrDatetime):
	flight = Structures.Flight(0,"f","f",depDatetime,arrDatetime,0)
	flight.flightId = flightId
	flight.depAirport = depAirport
	flight.arrAirport = arrAirport
	flight.depDatetime = depDatetime
	flight.arrDatetime = arrDatetime
	flight.flightDuration = arrDatetime - depDatetime
	return flight



def solutionsCreateList(numberOfChromosomes, totalSPs):
	solutions = [None]*numberOfChromosomes

	for k in xrange(0,len(solutions)):
		solutions[k] = solutionsAllocate(totalSPs)


	return solutions


def solutionsAllocate(totalSPs):

	# for x in xrange(0,totalSPs):
	solution = Structures.Solution(0,[],0.0)
	solution.solutionArray = [None]*totalSPs
	solution.ruleViolation = 0
	solution.fitness = 0.0

	return solution


def solutionsRandomInsert(solutions,N,numberOfChromosomes,totalSPs):
	for i in xrange(0,numberOfChromosomes):
		for k in xrange(0,totalSPs):
			solutions[i].solutionArray[k] = random.randint(1,N)

	return solutions


# def pilotKeyFunc(e):
# 	return e.SPArray[]


def keyFuncPilotSPs(e):
	return e.depDatetime


def spInsertToPilot(pilots, propIndex, NIndex, sp, SPList):  #allazei mono to SPArray kai to FTpilot
	SP = SPList[sp]

	pilots[propIndex][NIndex].SPArray.append(SP)
	pilots[propIndex][NIndex].FTpilot += float(SP.flightSPDuration.seconds)/float(3600) #se wres
	# print len(pilots[propIndex][NIndex].SPArray)

	return pilots


def setSPToPilots(solutions, pilots, N, numberOfChromosomes, totalSPs, SPList):
	for solutionNo in xrange(0,numberOfChromosomes):

		for spNo in xrange(0,totalSPs):  #edw eixa totalSPs -1. den kserw to logo. metrouse e ligotero sp
			# print spNo
			# print "pilot: ", solutions[solutionNo][spNo]
			pilots = spInsertToPilot(pilots, solutionNo, solutions[solutionNo].solutionArray[spNo]-1, spNo, SPList) # vazoume solutions[i][sp]-1 logw tou oti h arithmish twn keliwn ksekinaei apo 0



	return pilots


def clearPilotsList(pilots, flightDays):
	for solution in pilots:

		for pilot in solution:
			pilot.rule1Array = [0] * flightDays
			pilot.SPArray[:] = []
			pilot.FTpilot = 0.0

	return pilots



def printPilotsList(pilots, N, numberOfChromosomes):
	print "######PRINT PILOTSLIST######"

	for solutionNo in xrange(0,numberOfChromosomes):
		print "solution", solutionNo, ":"

		for pilotNo in xrange(0,N):
			print "	pilotID:", pilots[solutionNo][pilotNo].pilotId
			for x in pilots[solutionNo][pilotNo].SPArray:

				print "		spID:", x.spId

		print "\n"



def printSolutionsList(solutions, numberOfChromosomes, totalSPs, SPList):
	print "######PRINT SOLUTIONSLIST######"

	for solutionNo in xrange(0,numberOfChromosomes):
		print "solution", solutionNo, ":"
		for spNo in xrange(0,totalSPs):
			print "	[spNO:",SPList[spNo].spId, "]"
			print "		pilotID:", solutions[solutionNo].solutionArray[spNo]

		print "\n"


def printSP(spList):
	for i in spList:
		print i.spId
		print i.flightSPDuration

		for k in i.flightArray:
			print k.flightId
			print k.depAirport
			print k.arrAirport
			print k.depDatetime
			print k.arrDatetime
			print k.flightDuration
		print "\n"
