# from collections import namedtuple
import datetime as dt

# pilot = namedtuple("pilot", "SPArray FTpilot field3")
class Pilot:
    def __init__(self, pilotId, SPArray, rule1Array, FTpilot):
        self.pilotId = 0
        self.SPArray = []
        self.rule1Array = []
        self.FTpilot = 0.0


class SP:
	def __init__(self, spId, flightArray, flightSPDuration):
		self.spId = 0  #string
		self.flightArray = []
		self.flightSPDuration = dt.timedelta()
		self.numOfFlights = 0



class Flight:
	def __init__(self, flightId, depAirport, arrAirport, depDatetime, arrDatetime, flightDuration):
		self.flightId = 0
		self.depAirport = "f"
		self.arrAirport = "f"
		self.depDatetime = None
		self.arrDatetime = None
		self.flightDuration = 0


class Solution:
	def __init__(self, ruleViolation, solutionArray, fitness):
		self.ruleViolation = 0
		self.solutionArray = []
        fitness = 0.0
